# Bounty Hunting
<pretty image>


### What is it about
Ever wondered what it felt like having a bounty on your head in the Middle Ages? Let's experience that in this simulated futuristic Middle Ages! 

After a build up time depending on the size of the map, one or multiple players receive a bounty on their head. Other players gain score by damaging units (excluding walls) that belong to one of the players with a bounty on them. As a bounty player, fear not! Being wanted is prosperous and recognizeable. Players with a bounty on them gain a multiple of their income and become allied in order to allow them to survive, or even down right thrive through, the onslaught that is coming their way. All players get to have a bounty on them, the time a bounty lasts depends on the size of the map. The larger the map, the longer the bounty lasts.

The player with the most score at the end (disregarding the fact that the player may have died) is considered the winner.

This mod is an alternative to the standard free for all mode. Especially with uneven numbers, this mode can introduce a sense of direction of what to do.

### In-game settings
This mod does not offer any settings. It does overrule the setting whether teams are locked. The mod will lock the teams. It is best experienced with the Assisination victory condition.

### Installation guide
The standalone game Supreme Commander: Forged Alliance is required. This modification does not support the base game Supreme Commander. You do not need the base game.

Clone or download this repository. If downloaded, unzip the download. Open a new window and navigate towards your _My Documents_ folder. From there, navigate to: 
``` sh
".../My Documents/My Games/Gas Powered Games/Supreme Commander: Forged Alliance/Mods"
```
If the _Mods_ folder does not exist, create one. Move the folder _Bounty Hunting_ (not its contents) into the _Mods_ folder. The mod will now be available ingame.
