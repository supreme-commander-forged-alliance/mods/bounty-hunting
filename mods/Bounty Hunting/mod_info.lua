name = "Bounty Hunting"
uid = "bounty-hunting-2"
version = 2
copyright = ""
description = "Players will receive points for damaging players that have a bounty on them. The player with the most points at the end of the game wins!"
author = "(Jip) Willem Wijnia"
url = "https://gitlab.com/supreme-commander-forged-alliance/mods"

-- Icon made by photo3idea_studio Ware from www.flaticon.com
-- <div>Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/"                 title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/"                 title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
icon = "/mods/Bounty Hunting/icon.png"
selectable = true
enabled = true
exclusive = false
ui_only = false
requires = {}
requiresNames = {}
conflicts = {"74A9EAB2-E851-11DB-A1F1-F2C755D89593", "9d5cb32c-c87d-44b9-abec-13bf9ddfe983"}
before = {}
after = {}
