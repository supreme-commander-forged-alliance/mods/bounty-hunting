
local path = 'Bounty Hunting';

local UIUtil = import('/lua/ui/uiutil.lua');
local LayoutHelpers = import('/lua/maui/layouthelpers.lua');
local Group = import('/lua/maui/group.lua').Group;
local Bitmap = import('/lua/maui/bitmap.lua').Bitmap;
local Button = import('/lua/maui/button.lua').Button;
local Checkbox = import('/lua/maui/checkbox.lua').Checkbox;
local StatusBar = import('/lua/maui/statusbar.lua').StatusBar;
local GameMain = import('/lua/ui/game/gamemain.lua');
local Tooltip = import('/lua/ui/game/tooltip.lua');
local Prefs = import('/lua/user/prefs.lua');
local Tooltip = import('/lua/ui/game/tooltip.lua');

local parent = false;
interface = { };

function CreateModUI(isReplay, _parent)

	LOG("Creating UI!");

	parent = _parent

	local armies = FindApplicableArmies();

	BuildUI(armies)
	SetLayout(armies)
	CommonLogic(armies)

end

--------------------------------------------------
-- Retrieves all the armies that are controlled --
-- by a player. This is done in a similar       --
-- on the sim side.              				--

function FindApplicableArmies()

	-- return GetArmiesTable().armiesTable;

	-- an army needs to be human controlled in order
	-- to be applicable.
	local armies = GetArmiesTable().armiesTable;
	local applicableArmies = { };
	for k, army in armies do
		if army.human then
			table.insert(applicableArmies, army);
		end
	end

	return applicableArmies;

end

function BuildUI(armies)

	-- Create arrow checkbox
	interface.arrow = Checkbox(parent);

	-- Create group for main UI
	interface.box = Group(parent);
	
	-- create the panel.
	interface.box.panel = Bitmap(interface.box);

	interface.box.topPanel = Bitmap(interface.box);
	interface.box.middlePanel = Bitmap(interface.box);
	interface.box.bottomPanel = Bitmap(interface.box);

	-- Create the left bracket.
	interface.box.leftTopBracket = Bitmap(interface.box);
	interface.box.leftMiddleBracket = Bitmap(interface.box);
	interface.box.leftBottomBracket = Bitmap(interface.box);

	-- create the right 'bracket'.
	interface.box.rightGlowTop = Bitmap(interface.box);
	interface.box.rightGlowMiddle = Bitmap(interface.box);
	interface.box.rightGlowBottom = Bitmap(interface.box);
	
	interface.box.title = UIUtil.CreateText(interface.box, 'Bounty mode', 16, UIUtil.bodyFont);
	interface.box.title:SetDropShadow(true);

	interface.box.divider1 = Bitmap(interface.box);
	interface.box.divider2 = Bitmap(interface.box);

	interface.box.textTimer = UIUtil.CreateText(interface.box, 'Calm before the storm', 14, UIUtil.bodyFont);
	interface.box.bitmapTimer = Bitmap(interface.box);
	interface.box.bitmapProgress = Bitmap(interface.box);

	interface.box.textMultiplier = UIUtil.CreateText(interface.box, 'Income multiplier: ...', 14, UIUtil.bodyFont);

	--------------------------------------------------
	-- Prepare the description of the mod			--		

	interface.box.descriptions = { };
	local descriptions = {
		'Bounty players are displayed yellow',
		'and, if multiple, are allied.',
		'',
		'Bounty players receive a multiple of',
		'their income to increase their',
		'chances of surviving the onslaught.',
		'',
		'Damage done to bounty players',
		'provide points for the instignator.',
		'',
		'Damage to marked units provide full',
		'points, all other damage provides ',
		'1/4th of the points. Damage to ',
		'shields is ignored.',
		'',
		'Bounty players do not receive',
		'points until they lose the ',
		'bounty again.'
	};

	for k, description in descriptions do
		table.insert(interface.box.descriptions, UIUtil.CreateText(interface.box, description, 14, UIUtil.bodyFont));
	end

	--------------------------------------------------
	-- Make the individual player UI items.			--

	interface.box.armyData = { }
	for k, army in armies do 
		local data = { }

		-- satalite data
		data.color = army.color;
		data.faction = army.faction;
		data.nickname = army.nickname;

		-- actual UI elements
		data.icon = Bitmap(interface.box);
		data.iconBackground = Bitmap(interface.box);
		data.nickname = UIUtil.CreateText(interface.box, army.nickname, 14, UIUtil.bodyFont)
		data.points = UIUtil.CreateText(interface.box, '...', 14, UIUtil.bodyFont)
		table.insert(interface.box.armyData, data);
	end

end

function SetLayout(armies)

	--------------------------------------------------
	-- Make the little arrow to show / hide the		--
	-- panel										--

	interface.arrow:SetTexture(UIUtil.UIFile('/game/tab-l-btn/tab-close_btn_up.dds'))
	interface.arrow:SetNewTextures(UIUtil.UIFile('/game/tab-l-btn/tab-close_btn_up.dds'),
		UIUtil.UIFile('/game/tab-l-btn/tab-open_btn_up.dds'),
		UIUtil.UIFile('/game/tab-l-btn/tab-close_btn_over.dds'),
		UIUtil.UIFile('/game/tab-l-btn/tab-open_btn_over.dds'),
		UIUtil.UIFile('/game/tab-l-btn/tab-close_btn_dis.dds'),
		UIUtil.UIFile('/game/tab-l-btn/tab-open_btn_dis.dds'))
		
	LayoutHelpers.AtLeftTopIn(interface.arrow, GetFrame(0), -3, 172)
	interface.arrow.Depth:Set(function() return interface.box.Depth() + 10 end)

	--------------------------------------------------
	-- Make the panel and set its height according	--
	-- to the number of players.					--

	--interface.box.panel:SetTexture(UIUtil.UIFile('/game/resource-panel/resources_panel_bmp.dds'))
	interface.box.panel.Height:Set(20 + 14 * table.getn(armies) + 27 * 14);
	interface.box.panel.Width:Set(262);
	LayoutHelpers.AtLeftTopIn(interface.box.panel, interface.box)

	interface.box.Height:Set(interface.box.panel.Height)
	interface.box.Width:Set(interface.box.panel.Width)
	LayoutHelpers.AtLeftTopIn(interface.box, parent, 16, 153)
	
	interface.box:DisableHitTest()

	--------------------------------------------------
	-- Construct the actual panel					--

	interface.box.topPanel:SetTexture(UIUtil.UIFile('/game/score-panel/panel-score_bmp_t.dds'));
	interface.box.middlePanel:SetTexture(UIUtil.UIFile('/game/score-panel/panel-score_bmp_m.dds'));
	interface.box.bottomPanel:SetTexture(UIUtil.UIFile('/game/score-panel/panel-score_bmp_b.dds'));

	interface.box.topPanel.Depth:Set(interface.box.Depth() - 2);
	interface.box.middlePanel.Depth:Set(interface.box.Depth() - 2);
	interface.box.bottomPanel.Depth:Set(interface.box.Depth() - 2);

	interface.box.topPanel.Top:Set(function () return interface.box.Top() + 8 end);
	interface.box.topPanel.Left:Set(function () return interface.box.Left() + 8 end);
	interface.box.topPanel.Right:Set(function () return interface.box.Right() end);

	interface.box.bottomPanel.Top:Set(function () return interface.box.Bottom() end);
	interface.box.bottomPanel.Left:Set(function () return interface.box.Left() + 8 end);
	interface.box.bottomPanel.Right:Set(function () return interface.box.Right() end);

	interface.box.middlePanel.Top:Set(function () return interface.box.topPanel.Bottom() end);
	interface.box.middlePanel.Bottom:Set(function() return math.max(interface.box.bottomPanel.Top(), interface.box.topPanel.Bottom()) end)
	interface.box.middlePanel.Left:Set(function () return interface.box.Left() + 8 end);
	interface.box.middlePanel.Right:Set(function () return interface.box.Right() end);

	--------------------------------------------------
	-- Construct the left bracket					--

	interface.box.leftTopBracket:SetTexture(UIUtil.UIFile('/game/bracket-left/bracket_bmp_t.dds'))
	interface.box.leftMiddleBracket:SetTexture(UIUtil.UIFile('/game/bracket-left/bracket_bmp_m.dds'))
	interface.box.leftBottomBracket:SetTexture(UIUtil.UIFile('/game/bracket-left/bracket_bmp_b.dds'))

	interface.box.leftTopBracket.Top:Set(function () return interface.box.Top() + 2 end)
	interface.box.leftTopBracket.Left:Set(function () return interface.box.Left() - 12 end)

	interface.box.leftBottomBracket.Bottom:Set(function () return interface.box.Bottom() + 22 end)
	interface.box.leftBottomBracket.Left:Set(function () return interface.box.Left() - 12 end)

	interface.box.leftMiddleBracket.Top:Set(function () return interface.box.leftTopBracket.Bottom() end)
	interface.box.leftMiddleBracket.Bottom:Set(function() return math.max(interface.box.leftTopBracket.Bottom(), interface.box.leftBottomBracket.Top()) end)
	interface.box.leftMiddleBracket.Right:Set(function () return interface.box.leftTopBracket.Right() - 9 end)

	--------------------------------------------------
	-- Construct the right bracket					--

	interface.box.rightGlowTop:SetTexture(UIUtil.UIFile('/game/bracket-right-energy/bracket_bmp_t.dds'))
	interface.box.rightGlowMiddle:SetTexture(UIUtil.UIFile('/game/bracket-right-energy/bracket_bmp_m.dds'))
	interface.box.rightGlowBottom:SetTexture(UIUtil.UIFile('/game/bracket-right-energy/bracket_bmp_b.dds'))

	interface.box.rightGlowTop.Top:Set(function () return interface.box.Top() + 5 end)
	interface.box.rightGlowTop.Left:Set(function () return interface.box.Right() - 10 end)

	interface.box.rightGlowBottom.Bottom:Set(function () return interface.box.Bottom() + 20 end)
	interface.box.rightGlowBottom.Left:Set(function () return interface.box.rightGlowTop.Left() end)

	interface.box.rightGlowMiddle.Top:Set(function () return interface.box.rightGlowTop.Bottom() end)
	interface.box.rightGlowMiddle.Bottom:Set(function () return math.max(interface.box.rightGlowTop.Bottom(), interface.box.rightGlowBottom.Top()) end)
	interface.box.rightGlowMiddle.Right:Set(function () return interface.box.rightGlowTop.Right() end)

	--------------------------------------------------
	-- Make the individual player UI items.			--

	for k, data in interface.box.armyData do 

		-- construct the icon
		data.icon:SetTexture(UIUtil.UIFile(UIUtil.GetFactionIcon(data.faction)));
		data.icon.Width:Set(14);
		data.icon.Height:Set(14);
		LayoutHelpers.AtLeftTopIn(data.icon, interface.box, 20, 18 + k * 14)

		-- construct the background of the icon, used for coloring.
		data.iconBackground:SetSolidColor(data.color);
		data.iconBackground.Width:Set(14);
		data.iconBackground.Height:Set(14);
		data.iconBackground.Depth:Set(data.icon.Depth() - 1);
		LayoutHelpers.AtLeftTopIn(data.iconBackground, interface.box, 20, 18 + k * 14)
		
		data.nickname:SetColor('ffffffff');
		LayoutHelpers.AtLeftTopIn(data.nickname, interface.box, 40, 18 + k * 14)

		data.points:SetColor('ffffffff');
		LayoutHelpers.AtRightTopIn(data.points, interface.box, 15, 18 + k * 14)
	end

	--------------------------------------------------
	-- Make the title, editable text, dividers and	--
	-- the timer									--

	LayoutHelpers.AtLeftTopIn(interface.box.title, interface.box, 20, 11)
	interface.box.title:SetColor('ffffaa55')

	-- due to the title and the top portion of the panel ('box').
	local offsetY = 31;

	--interface.box.divider:SetTexture(UIUtil.UIFile('/game/bracket-right-energy/bracket_bmp_m.dds'));
	interface.box.divider1:SetSolidColor('aaaaaaaa');
	LayoutHelpers.AtLeftTopIn(interface.box.divider1, interface.box, 20, offsetY + 14 * table.getn(armies) + 1 * 14);
	interface.box.divider1.Width:Set(222);
	interface.box.divider1.Height:Set(1);

	LayoutHelpers.AtLeftTopIn(interface.box.textMultiplier, interface.box, 20, offsetY + 14 * table.getn(armies) + 2 * 14);
	interface.box.textMultiplier:SetColor('ffcccccc')

	LayoutHelpers.AtLeftTopIn(interface.box.textTimer, interface.box, 20, offsetY + 14 * table.getn(armies) + 4 * 14);
	interface.box.textTimer:SetColor('ffcccccc')

	LayoutHelpers.AtLeftTopIn(interface.box.bitmapTimer, interface.box, 20, (offsetY + 2) + 14 * table.getn(armies) + 5 * 14);
	interface.box.bitmapTimer:SetSolidColor('aaaaaaaa');
	interface.box.bitmapTimer.Width:Set(222);
	interface.box.bitmapTimer.Height:Set(10);

	LayoutHelpers.AtLeftTopIn(interface.box.bitmapProgress, interface.box, 22, (offsetY + 4) + 14 * table.getn(armies) + 5 * 14);
	interface.box.bitmapProgress:SetSolidColor('fff9e79f');
	interface.box.bitmapProgress.Width:Set(218);
	interface.box.bitmapProgress.Height:Set(6);

	interface.box.divider2:SetSolidColor('aaaaaaaa');
	LayoutHelpers.AtLeftTopIn(interface.box.divider2, interface.box, 20, offsetY + 14 * table.getn(armies) + 7 * 14);
	interface.box.divider2.Width:Set(222);
	interface.box.divider2.Height:Set(1);

	--------------------------------------------------
	-- Make all the descriptions					--

	local rowOffset = 7;
	local absoluteOffset = 31;
	for k, uiDescription in interface.box.descriptions do
		LayoutHelpers.AtLeftTopIn(uiDescription, interface.box, 20, absoluteOffset + ((k + rowOffset) + table.getn(armies)) * 14);
		uiDescription:SetColor('ffcccccc')
	end
end

function CommonLogic(armies)

	--  Button Actions
	interface.arrow.OnCheck = function(self, checked)
		TogglePanel()
	end

	GameMain.AddBeatFunction(
		ProgressBarBeat
	);
	
end

function ProgressBarBeat(UIElement)



end

function TogglePanel(state)

	if import('/lua/ui/game/gamemain.lua').gameUIHidden and state != nil then
		return
	end

	if UIUtil.GetAnimationPrefs() then
		if state or interface.box:IsHidden() then
			PlaySound(Sound({Cue = "UI_Score_Window_Open", Bank = "Interface"}))
			interface.box:Show()
			ShowHideElements(true)
			interface.box:SetNeedsFrameUpdate(true)
			interface.box.OnFrame = function(self, delta)
				local newLeft = self.Left() + (1000*delta)
				if newLeft > parent.Left()+14 then
					newLeft = parent.Left()+14
					self:SetNeedsFrameUpdate(false)
				end
				self.Left:Set(newLeft)
			end
			interface.arrow:SetCheck(false, true)
		else
			PlaySound(Sound({Cue = "UI_Score_Window_Close", Bank = "Interface"}))
			interface.box:SetNeedsFrameUpdate(true)
			interface.box.OnFrame = function(self, delta)
				local newLeft = self.Left() - (1000*delta)
				if newLeft < parent.Left()-self.Width() then
					newLeft = parent.Left()-self.Width()
					self:SetNeedsFrameUpdate(false)
					self:Hide()
					ShowHideElements(false)
				end
				self.Left:Set(newLeft)
			end
			interface.arrow:SetCheck(true, true)
		end
	else
		if state or interface.box:IsHidden() then
			interface.box:Show()
			ShowHideElements(true)
			interface.arrow:SetCheck(false, true)
		else
			interface.box:Hide()
			ShowHideElements(false)
			interface.arrow:SetCheck(true, true)
		end
	end
end

function ShowHideElements(show)

	if show then
		for k, data in interface.box.armyData do 
			data.icon:Show();
			data.iconBackground:Show();
			data.nickname:Show();
			data.points:Show();
		end
	else

		for k, data in interface.box.armyData do 
			data.icon:Hide();
			data.iconBackground:Hide();
			data.nickname:Hide();
			data.points:Hide();
		end
	end
end