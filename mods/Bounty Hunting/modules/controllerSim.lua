
local path = 'Bounty Hunting'

local ScenarioUtils = import('/lua/sim/ScenarioUtilities.lua');
local ScenarioFramework = import('/lua/ScenarioFramework.lua');

local state = {
    GracePeriodTime = 60,
    Multiplier = 1.4,

    -- take note: these are overridden.
    Threshold = 5000,
    StartupTime = 480,
    BountyTime = 600,
}

function OnStart()

    -- prevent alliances to be made or broken.
    ScenarioInfo.TeamGame = true
    Sync.LockTeams = true

    -- find all human players. Ai's do not participate.
    applicableBrains = FindApplicableBrains();
    PrepareBrains(applicableBrains);

    -- set some values depending on the map size
    state.BountyTime = 240 + 20 * math.floor(GetMapSize() / 128);
    state.StartupTime = 240 + 30 * math.floor(GetMapSize() / 256);
    state.Theshold = 5000 + 1000 * math.floor(GetMapSize() / 64);

    LOG("-------------------");
    LOG(repr(state.Theshold))

    -- early game message.
    ScenarioFramework.CreateTimerTrigger(
        function()

            Sync.SendTimer = { Title = "Time before bounties are set", Time = state.StartupTime - 10 };

            -- start updating the UI thread.
            ForkThread(SendStateToUIThread, applicableBrains);        
            ForkThread(UpdateMultiplierThread);

            Sync.SendAnnouncement = { Title = "Bounty mode", SubTitle = "Bounties will be set in " .. math.floor(state.StartupTime / 60) .. " minutes." };
        end,
        10,
        true
    );

    -- one minute warning
    ScenarioFramework.CreateTimerTrigger(
        function()
            Sync.SendAnnouncement = { Title = "Bounty mode", SubTitle = "Bounties will be set in one minute." };
        end,
        state.StartupTime - 60,
        true
    );

    -- whoooo!!!
    ScenarioFramework.CreateTimerTrigger(
        function()

            -- start allowing people to have more income.
            for k, brain in applicableBrains do
                ForkThread(BountyMultiplierThread, brain);
            end

            -- start the ticking thread to set bounties!
            ForkThread(WinConditionsThread, applicableBrains);
            ForkThread(TickThread, applicableBrains);
        end,
        state.StartupTime,
        true
    );

end

--------------------------------------------------
-- Retrieves all the armies that are controlled --
-- by a player. This is done in a similar       --
-- on the ui side.              				--

function FindApplicableBrains()

    -- return ArmyBrains;

    local applicableBrains =  { };
    for k, brain in ArmyBrains do

        if not (brain.BrainType == "AI") then
            table.insert(applicableBrains, brain);
        end

    end

    return applicableBrains;
end

--------------------------------------------------
-- Adds in information to the brains to prepare --
-- the mod. This works because Lua works with   --  
-- pass by reference for tables.                --

function PrepareBrains(brains)

    for k, brain in brains do
        brain.BountyMode = {
            ArmyIndex = brain:GetArmyIndex(),
            Score = 0,
            IsBounty = false,
        }
    end
end

--------------------------------------------------
-- Extracts the bounty data from the brains in  --
-- order to transfer it to the UI. Take note    --
-- of the deep copy, removing the implicit      --
-- reference to the original data.              --

function ExtractDataFromBrains(brains)

    local data = { };

    for k, brain in brains do
        local datum = table.deepcopy(brain.BountyMode)
        datum.IsDefeated = brain:IsDefeated();
        table.insert(data, datum);
    end

    return data;
end

--------------------------------------------------
-- Sends the data to the UI thread for          --
-- visualisation of the state.                  --

function SendStateToUIThread(brains)

    while true do
        Sync.SendScoreData = ExtractDataFromBrains(brains);
        WaitSeconds(0.25);
    end

end

--------------------------------------------------
-- Updates the multiplier and sends the data to --
-- the UI thread.                               --

function UpdateMultiplierThread()

    WaitSeconds(1.0);

    while true do

        state.Multiplier = 1.4 + 0.025 * (math.floor(GetGameTimeSeconds() / 60));
        Sync.SendMultiplier = { Multiplier = state.Multiplier };
        WaitSeconds(60);
    end

end

--------------------------------------------------
-- Given the list of brains, sends back the     --
-- brains that have not been defeated yet.      --

function FindUndefeatedBrains(brains)

    local undefeatedBrains =  { };
    for k, brain in brains do
        if not brain:IsDefeated() then
            table.insert(undefeatedBrains, brain);
        end
    end

    return undefeatedBrains;
end

function WinConditionsThread(brains)

    while true do

        -- find all the undefeated brains.
        local undefeatedBrains = FindUndefeatedBrains(brains);

        -- if there is just one brain left, then we can choose a winner!
        if table.getn(undefeatedBrains) <= 1 then

            local winner = brains[1];
            for k, brain in brains do
                if brain.BountyMode.Score > winner.BountyMode.Score then
                    winner = brain;
                end
            end

            Sync.SendAnnouncement = {Title = "Bounty mode", SubTitle = "Commander " .. winner.Nickname .. " is the winner!"};
            break;
        end

        WaitSeconds(1.0);

    end
end

--------------------------------------------------
-- Given a list of brains, sum all the t1, t2,  --
-- t3 and experimental units.                   --

function SumAllTechs(brains)

    -- the value to add for a given tech
    -- local t1 = 30;
    -- local t2 = 140;
    -- local t3 = 720;
    -- local t4 = 12050;

    -- todo: find per brain all tech levels
    -- local value = 0;

    local techs = {
        t1 = 0,
        t2 = 0,
        t3 = 0,
        t4 = 0,
    }

    for k, brain in brains do
        local units = brain:GetListOfUnits();
    end

    for k, brain in brains do
        if not brain:IsDefeated() then
            table.insert(undefeatedBrains, brain);
        end
    end

    return undefeatedBrains;
end

--------------------------------------------------
-- The heart of the operation. Ensures bounties --
-- are set and removed on time.                 --

function TickThread(brains)

    function BrainToIndices(brains)

        local indices = { };
        for k, brain in brains do
            table.insert(indices, brain:GetArmyIndex());
        end

        return indices;
    end

    function SetAlliances(brains)

        for ka, a in brains do
            for kb, b in brains do

                -- AI's do not participate.
                if a.BountyMode and b.BountyMode then

                    local ia = a:GetArmyIndex();
                    local ib = b:GetArmyIndex();

                    -- if we're not the same, we become allies if we're both
                    -- a bounty target. Otherwise we become hostile.
                    if not (ia == ib) then
                        if a.BountyMode.IsBounty and b.BountyMode.IsBounty then
                            SetAlliance(ia, ib, 'Ally');
                        else
                            SetAlliance(ia, ib, 'Enemy');
                        end
                    end
                end
            end
        end
    end

    -- determine the number of brains that are still playing.
    local brainIndices = BrainToIndices(brains);
    local pRandom = import("/mods/" .. path .. "/modules/pseudoRandom.lua").PseudoRandom:Initiate(brainIndices);

    -- find the brains that are not defeated yet, then compute
    -- the total amount of bounty players.
    local undefeatedBrains = FindUndefeatedBrains(brains);
    local numberOfBountyPlayers = 1 + math.floor((table.getn(undefeatedBrains) - 1.5) / 3);

    -- keep doing this until only one player is alive at the end.
    repeat 
    
        -- make a few players the bounty!
        for k = 1, numberOfBountyPlayers do

            -- find a brain that is not already a bounty and is not defeated either.
            local brain = ArmyBrains[pRandom:GetValue()];
            while brain.BountyMode.IsBounty or brain:IsDefeated() do
                brain = ArmyBrains[pRandom:GetValue()];
            end

            brain.BountyMode.IsBounty = true;
        end

        -- wait a bit for the onslaughter to happen.
        SetAlliances(brains);

        Sync.SendAnnouncement = { Title = "Bounty mode", SubTitle = "The bounties have been set!"};
        Sync.SendTimer = { Title = "Bounty hunting time!", Time = state.BountyTime };
        
        WaitSeconds(state.BountyTime);

        -- make all players 'regular'
        for k, brain in brains do
            brain.BountyMode.IsBounty = false;
        end

        -- wait a bit for a 'grace' period
        SetAlliances(brains);

        Sync.SendAnnouncement = { Title = "Bounty mode", SubTitle = "The bounties have been removed!"};
        Sync.SendTimer = { Title = "Grace period", Time = state.GracePeriodTime };

        -- set the new threshold during the grace period
        state.Threshold = state.Threshold + 

        WaitSeconds(60);

        -- determine the new amount of bounty players
        undefeatedBrains = FindUndefeatedBrains(brains);
        numberOfBountyPlayers = 1 + math.floor((table.getn(undefeatedBrains) - 1.5) / 3);

    until table.getn(undefeatedBrains) == 1;
end

--------------------------------------------------
-- A player with the bounty receives            --
-- a multiplication of its economy.             --

function BountyMultiplierThread(brain)

    --------------------------------------------------
    -- Applies the multiplier to all the production --
    -- values of the given units units.             --

    function ApplyResourceMultiplier(units, multiplier)

        -- for every non-dead unit...
        for k, unit in units do
            if not unit:IsDead() then
                
                -- get the original production values and then apply the modifier to it!
                local bp = unit:GetBlueprint();
                
                local energy = bp.Economy.ProductionPerSecondEnergy;
                if energy then
                    unit:SetProductionPerSecondEnergy(energy * multiplier);
                end

                local mass = bp.Economy.ProductionPerSecondMass;
                if mass then 
                    unit:SetProductionPerSecondMass(mass * multiplier);
                end
            end
        end
    end

    local markers = { };

    -- places fancy objective markers on the given units.
    function AddObjectiveMarkers(units)

        for k, unit in units do
            if unit and not unit:IsDead() and not unit.HasMarkerAttached then

                LOG(unit);

                local ObjectiveArrow = import('/lua/objectiveArrow.lua').ObjectiveArrow
                local arrow = ObjectiveArrow { AttachTo = unit, Size = 1.0 }

                unit.HasMarkerAttached = true;
                table.insert( markers, arrow);

            end
        end
    end

    -- remove the fancy markers again.
    function RemoveObjectiveMarkers(units)
        for k, marker in markers do
            marker:Destroy();
        end

        for k, unit in units do
            unit.HasMarkerAttached = false;
        end
    end

    while true do
        
        if brain.BountyMode.IsBounty then

            -- While we're a bounty, keep providing the bonus
            -- for current and future structures. And add fancy markers!
            while brain.BountyMode.IsBounty do
                local units = 
                ApplyResourceMultiplier(
                    brain:GetListOfUnits(categories.MASSPRODUCTION + categories.ENERGYPRODUCTION, false), 
                    state.Multiplier
                );

                AddObjectiveMarkers(
                    brain:GetListOfUnits(
                        categories.ENGINEER + categories.COMMAND + (categories.STRUCTURE - (categories.DIRECTFIRE + categories.SHIELD)), false)
                );

                WaitSeconds(2.0);
            end

            -- We're no longer a bounty, remove the
            -- provided bonus. And remove the fancy markers.
            ApplyResourceMultiplier(
                brain:GetListOfUnits(categories.MASSPRODUCTION + categories.ENERGYPRODUCTION, false),
                1.0
            );

            RemoveObjectiveMarkers(brain:GetListOfUnits(categories.ENGINEER + categories.COMMAND + (categories.FACTORY - categories.MOBILE), false));
            
        end

        WaitSeconds(1.0);
    end
end

