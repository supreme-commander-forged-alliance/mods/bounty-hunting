
local announcement = import('/lua/ui/game/announcement.lua');
local GameMain = import('/lua/ui/game/gamemain.lua');

local path = 'Bounty Hunting';
local interface = import('/mods/' .. path .. '/modules/interface.lua').interface;


--  { 
--      { ArmyIndex = number, Score = number, IsDefeated = bool, IsBounty = bool },
--      { ArmyIndex = number, Score = number, IsDefeated = bool, IsBounty = bool },
--      ...
--  }
function ProcessScoreData(data)

    for k, datum in data do
        interface.box.armyData[k].points:SetText(string.format("%i", datum.Score));

        if datum.IsDefeated then
            interface.box.armyData[k].nickname:SetColor('ff999999');
        else
            local color = 'ffffffff';
            
            if datum.IsBounty then
                color = 'fff9e79f';
            end

            interface.box.armyData[k].nickname:SetColor(color);
            interface.box.armyData[k].points:SetColor(color)
        end
    end
end

-- {
--     Multiplier = number,
-- }
function ProcessMultiplier(data)
    interface.box.textMultiplier:SetText(string.format("Income multiplier: %0.3f", data.Multiplier));
end

-- {
--     Title = string,
--     SubTitle = string
-- }
function ProcessAnnouncement(data)
    announcement.CreateAnnouncement(data.Title, interface.arrow, data.SubTitle);
end


-- {
--     Title = string,
--     Time = number (seconds)
-- }
local gameBeatFunction = nil;
function ProcessTimer(data)

    if gameBeatFunction then
        GameMain.RemoveBeatFunction(gameBeatFunction)
    end

    -- oh noes! hardcoded!
    local totalWidth = 218;
    local totalTime = data.Time;
    local startTime = GetGameTimeSeconds();

    interface.box.textTimer:SetText(data.Title);

    gameBeatFunction = function()

        local delta = math.max(totalTime - (GetGameTimeSeconds() - startTime), 0);
        local progress = totalWidth * (delta / totalTime);
        interface.box.bitmapProgress.Width:Set(progress);

    end

    GameMain.AddBeatFunction(gameBeatFunction);
end