

local baseOnSync = OnSync

function OnSync()

    -- don't break anything!
    baseOnSync()
    
    local path = 'Bounty Hunting'
    local controllerUI = import('/mods/' .. path .. '/modules/controllerUI.lua');

    if Sync.SendScoreData then
        controllerUI.ProcessScoreData(Sync.SendScoreData);
    end

    if Sync.SendAnnouncement then
        controllerUI.ProcessAnnouncement(Sync.SendAnnouncement);
    end

    if Sync.SendMultiplier then
        controllerUI.ProcessMultiplier(Sync.SendMultiplier);
    end

    if Sync.SendTimer then
        controllerUI.ProcessTimer(Sync.SendTimer);
    end
end 