do

    local oUnit = Unit;
    Unit = Class(oUnit)
    {
        DoTakeDamage = function(self, instigator, amount, vector, damageType)
        
            -- if there is a source, may not always be the case!
            -- do not provide points for damage to walls.
            if instigator and not EntityCategoryContains(categories.WALL, self) then

                -- retrieve the brains and their indices.
                local brainI = instigator:GetAIBrain();

                -- is our instigator actually part of the mod? And if so,
                -- is it not self a wanted person?
                if brainI.BountyMode and not brainI.BountyMode.IsBounty then

                    local brainS = self:GetAIBrain();
                    local indexS = brainS:GetArmyIndex();
                    local indexI = brainI:GetArmyIndex();

                    -- if we are a bounty, then provide points to the instigator!
                    if brainS.BountyMode.IsBounty then

                        -- 'regular' units provide a portion of the points.
                        local multiplier = 0.25;
                        local cat = categories.ENGINEER + categories.COMMAND + (categories.STRUCTURE - (categories.DIRECTFIRE + categories.SHIELD));
                        -- marked units provide full points.
                        if EntityCategoryContains(cat, self) then
                            multiplier = 1.0;
                        end
                        
                        brainI.BountyMode.Score = brainI.BountyMode.Score + math.min(multiplier * amount, self:GetHealth());
                    end
                end
            end

            -- do the actual damage :))
            oUnit.DoTakeDamage(self, instigator, amount, vector, damageType);
        end
    }
end