



local parentBeginSession = BeginSession
function BeginSession()

    -- dun break anything!
    parentBeginSession();

    local path = 'Bounty Hunting'
    local controllerSim = import('/mods/' .. path .. '/modules/controllerSim.lua');

    -- run our own bit of the sim!
    ForkThread(
        function()
            controllerSim.OnStart();
        end
    );

end